# Developer Instructions

TLDR; Changes to the script should be made in a new feature branch, merged into master, tagged with a version number
and stable tag should be updated.

## Importing script into TamperMonkey for testing purposes
You can force TamperMonkey to load the modified script for testing purposed by visiting
`https://gitlab.ebi.ac.uk/ebiwd/rthelper/raw/<tag-or-branch>/rthelper.user.js`

- if your feature branch is `feature-report-spam`, then you can import the script into TamperMonkey from
  https://gitlab.ebi.ac.uk/ebiwd/rthelper/raw/feature-report-spam/rthelper.user.js
- if your tagged version is `2.7.0`, then you can import the script into TamperMonkey from
  https://gitlab.ebi.ac.uk/ebiwd/rthelper/raw/2.7.0/rthelper.user.js
- there is a special 'stable' tag that is applied to the most recent tested version - this is used as the update
  endpoint by TamperMonkey and available at https://gitlab.ebi.ac.uk/ebiwd/rthelper/raw/stable/rthelper.user.js

Don't forget to restore the original stable code when you are finished testing alternative versions.

# Release procedure
- Ensure the `@version` metadata in `rthelper.user.js` is updated according to your updates - this should be updated
  according to semantic versioning standards
- Merge feature branch to master branch
- Tag the master branch with the same version number
- Delete the existing `stable` tag
- Re-apply the `stable` tag to the same commit as the most recent version
- TamperMonkey will check for script updates periodically, it will notice the version number increase and update the
  script accordingly (there is no automatic downgrade)
