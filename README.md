# RTHelper

This script provides a number of user enhancements to the default interface of RT at EBI, to enable quicker and easier management of tickets.

The functionality is implemented in injecting a script into pages from RT - to do this we use a browser plugin called TamperMonkey.

_Unfortunately, this script does not work in Safari, as Apple has blocked some of the commands._

_Please let me know if you try this with Opera, Edge, Dolphin or UC, by creating an issue in the [Issue Queue](https://gitlab.ebi.ac.uk/ebiwd/rthelper/issues)_

## Installation

### 1. Install plugin framework

If you do not have TamperMonkey or GreaseMonkey installed, please install the browser plugin to provide the framework for intersting and activating user scripts on a webpage.

[Install TamperMonkey](https://www.tampermonkey.net) for your browser:
[Chrome](https://tampermonkey.net/?ext=dhdg&browser=chrome) / [Firefox](https://tampermonkey.net/?ext=dhdg&browser=firefox)

### 2. Install/Update script

Once you have installed GreaseMoney or TamperMonkey, please click on the link below - TamperMonkey/GreaseMonkey will ask you to approve the script.

[Install RTHelper](https://gitlab.ebi.ac.uk/ebiwd/rthelper/raw/stable/rthelper.user.js)

### 3. Launch RT

Once installed, visit https://helpdesk.ebi.ac.uk/ where the script will activate and automatically take you to the configuration page. Set the values as appropraite, and click 'Submit' at the bottom of the page.

## Bugs & Suggestions

This script was developed by [Peter Walter](https://www.ebi.ac.uk/about/people/peter-walter) in the [Web Development Team](https://www.ebi.ac.uk/about/people/jonathan-hickford).
Please email bugs & suggestions to www-dev@ebi.ac.uk or raise an issue in the [Issue Queue](https://gitlab.ebi.ac.uk/ebiwd/rthelper/issues)
