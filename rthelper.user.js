// ==UserScript==
// @name RT Helpers
// @namespace https://helpdesk.ebi.ac.uk/
// @description Add some shortcut links to RT
// @match https://helpdesk.ebi.ac.uk/*
// @include https://helpdesk.ebi.ac.uk/*
// @grant GM_xmlhttpRequest
// @version 2.7.4
// @updateURL https://gitlab.ebi.ac.uk/ebiwd/rthelper/raw/stable/rthelper.user.js
// @downloadURL https://gitlab.ebi.ac.uk/ebiwd/rthelper/raw/stable/rthelper.user.js
// @supportURL https://gitlab.ebi.ac.uk/ebiwd/rthelper/issues
// @run-at document-end
// ==/UserScript==

var info = {};
info.version = GM_info.script.version;
info.domain = 'https://helpdesk.ebi.ac.uk';
info.path = '';
// get 'nobody' id from https://helpdesk.ebi.ac.uk/REST/1.0/user/nobody
info.nobody = '6';

// add trim to String prototype
if (!String.prototype.trim) {
  String.prototype.trim = function() {
    return this.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
  };
}

if (!String.prototype.tokenize) {
  String.prototype.tokenize = function(token, count) {
    var stringSplit = this.split(token);
    var returnArray = [];
    for (var i=0; i<count-1; i++) {
      returnArray[i] = stringSplit.shift();
    }
    returnArray[count-1] = stringSplit.join(token);
    return returnArray;
  };
}

/* jshint ignore:start */
// add password encoding/decoding prototypes
eval(function(p,a,c,k,e,d){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--){d[e(c)]=k[c]||e(c)}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}('z(!6.7.w&&!6.7.u&&!6.7.n&&!6.7.p){6.7.p=h(){3 l=\'\';q(3 i=0;i<4.d;i++){l+=8.c()>0.5?4[i].A():4[i]}k l};6.7.n=h(o){3 a=8.c()>0.5?4:o;3 b=a===4?o:4;3 i=8.s(8.c()*(a.d+1));3 j=8.s(8.c()*(b.d+1));k a.e(0,i)+b.e(0,j)+a.e(i)+b.e(j)};6.7.w=h(){3 9=\'\';q(3 i=0;i<4.d;i++){9+=4.x(i).t(B).n(8.c().t(D).C(2).r(/[0-v-f]/g,\'\')).p()}k 9};6.7.u=h(){3 m=\'\';3 9=4.r(/[^0-v-f]/E,\'\');q(3 i=0;i<9.d;i+=2){m+=\'%\'+9.e(i,2)}k y(m)}}',41,41,'|||var|this||String|prototype|Math|encoded|||random|length|substr|||function|||return|mixcased|decoded|interleave|string|mixcase|for|replace|floor|toString|decode|9a|encode|charCodeAt|unescape|if|toUpperCase|16|substring|36|gi'.split('|'),0,{}))
/* jshint ignore:end */

// utility function for removing dom element
function removeElement(elem) {
  if (elem.parentNode) {
    elem.parentNode.removeChild(elem);
  }
}
// utiliy functions for localstorage
if (typeof deleteValue === 'undefined') {
  deleteValue = function(name) {
    localStorage.removeItem(name);
  };
  getValue = function(name, defaultValue) {
    var value = localStorage.getItem(name);
    if (!value)
      return defaultValue;
    var type = value[0];
    value = value.substring(1);
    switch (type) {
      case 'b':
        return value == 'true';
      case 'n':
        return Number(value);
      default:
        return value;
    }
  };
  setValue = function(name, value) {
    value = (typeof value)[0] + value;
    localStorage.setItem(name, value);
  };
}

/**
 * Get value from html element
 * @param String selector
 * @param String type
 * @return String
 */
function getElementText(selector, type) {
  var val = '';
  var el;
  selector = selector || '';
  type = type || 'innerHTML';

  el = document.querySelector(selector);
  if (!el) {
    return '';
  }

  switch(type) {
    case 'href':
      return el.href.trim();
    case 'text':
      return (el.innerText || el.textContent).trim();
    default:
    case 'innerHTML':
      return el.innerHTML.trim();
  }
}

/**
 * Get id of current user
 * @return String, eg "19200"
 */
function getUserId() {
  if (getValue('userId', '') === '') {
    setValue('userId', getElementText('a#preferences', 'href').split('=')[1]);
  }
  return getValue('userId', '');
}

/**
 * Get id for current ticket
 * @return String, eg "17774"
 */
function getTicketId() {
  var raw_value = getElementText('div.ticket-info-basics div.id div.value').split('\n',1)[0];
  var ticket_id = raw_value.replace(/<\/?span[^>]*>/g,"");
  return ticket_id;
}

/**
 * Get name of current user
 * @param Bool full - true to return fullname, default false to return firstname
 * @return String, eg "Peter" or "Peter Walter"
 */
function getName(full) {
  if (getValue('name', '') === '') {
    setValue('name', getElementText('div.RealName span.value'));
  }
  return full ? getValue('name', '') : getValue('name', '').split(' ',1)[0];
}

/**
 * Get queue for current ticket
 * @return String, eg "Web Development"
 */
function getQueue() {
  return getElementText('div.ticket-info-basics div.queue span.current-value', 'text').split('\n')[0];
}

/**
 * Get owner for current ticket
 * @return String, eg
 */
function getOwner() {
  var owner=getElementText('div.ticket-info-people span.current-value', 'text').split('\n')[0];
  if (owner.indexOf('(') !== -1) {
    owner=/\((.*)\)/gi.exec(owner)[1];
  }
  return owner;
}

/**
 * Get custom field for current ticket
 * @return String
 */
function getCustomField(id) {
  var field_val =  getElementText('div#CF-' + id + '-ShowRow span.current-value', 'text').split('\n',1)[0];
  return field_val;
}

/**
 * Get status for current ticket
 * @return String, eg "Open", "Resolved"
 */
function getStatus() {
  return getElementText('div.ticket-info-basics div.status div.value').split('\n',1)[0];
}

/**
 * Get modify link for current ticket
 * @param Object commands - object of argument key value pairs to append to modify link
 * @return String
 */
function getModifyLink(commands) {
  var arg = '';

  if (typeof commands === 'object') {
    for(var key in commands) {
      if (key && commands[key]) {
        arg += "&" + key.toString().trim() + "=" + encodeURIComponent(commands[key].toString().trim());
      }
    }
  }

  return getElementText('div#page-navigation a#page-basics', 'href') + arg;
}

/**
 * Get modify people link for current ticket
 * @param Object commands - object of argument key value pairs to append to modify people link
 * @return String
 */
function getModifyPeopleLink(commands) {
  var arg = '';

  if (typeof commands === 'object') {
    for(var key in commands) {
      if (key && commands[key]) {
        arg += "&" + key.toString().trim() + "=" + commands[key].toString().trim();
      }
    }
  }

  return getElementText('div#page-navigation a#page-people', 'href') + arg;
}

/**
 * Get most recent reply link for current ticket
 * @param Object commands - array of argument key value pairs to append to reply link
 * @return String
 */
function getReplyLink(commands) {
  var arg = '';

  if (typeof commands === 'object') {
    for(var key in commands) {
      if (key && commands[key]) {
        arg += "&" + key.toString().trim() + "=" + commands[key].toString().trim();
      }
    }
  }

  return getElementText('.message .reply-link', 'href') + arg;
}

/**
 * Insert node into DOM
 * @param DocumentObject container
 * @param String text - link text
 * @param String link - link href, null for text only
 * @param String prefix - text before link
 * @param String suffix - text after link
 * @param Bool breakBefore - true to place on new line
 * @return DocumentObject
 */
function insertLink(container, text, link, prefix, suffix, breakBefore) {
  var newlink = null;

  if (!container) {
    return null;
  }

  if (breakBefore) {
    container.appendChild(document.createElement('br'));
  }

  if (prefix) {
    container.appendChild(document.createTextNode(prefix));
  }

  if (link) {
    newlink = document.createElement('a');
    newlink.innerHTML = text;
    newlink.href = link;
  }
  else {
    newlink = document.createTextNode(text);
  }
  container.appendChild(newlink);

  if (suffix) {
    container.appendChild(document.createTextNode(suffix));
  }

  return newlink;
}

/**
 * Build javascript link
 * @param String url - url of action
 * @param String done - function to call when complete
 * @return String
 */
function buildLink(url, done) {
  return (
    'javascript:(' +
    'function(){' +
    'jQuery.ajax({' +
    'url:\'' + url + '\',' +
    'cache:false,' +
    'complete:' + done +
    '});' +
    '}' +
    ')();'
  );
}

/**
 * Refresh entire page
 * @return String
 */
function refreshElement() {
  return (
    'function(){' +
    'location.reload();' +
    '}'
  );
}

function getObject(name) {
  var returnObject = {};

  var keys = getValue(name, '').split(';');
  for (var key in keys) {
    if (keys[key]) {
      var item = keys[key].split(':');
      returnObject[item[0]] = item[1];
    }
  }

  return returnObject;
}
function getQueues() {
  return getObject('showQueues');
}
function getOwners() {
  return getObject('showOwners');
}
function getStatuses() {
  return getObject('showStatuses');
}


function getCustomFields(cfId) {
  var oldValue = '';
  // get new values of customField every 5 min
  if (getValue('customFields-' + cfId + '-timestamp', 0) < (new Date().getTime()/1000 - 5*60)) {
    // refresh list of customFields
    GM_xmlhttpRequest({
      method: "GET",
      url: getModifyLink(),
      onload: function(response) {
        var customFields = {};
        var customFieldsString = 'Default-None:(no value);';

        var temp = document.createElement('div');
        temp.innerHTML = response.responseText;
        var optGroup = 'Default';
        var optValue = '';
        var cfOptions = temp.querySelectorAll('select[name="Object-RT::Ticket-' + getTicketId() + '-CustomField-' + cfId + '-Values"] *');
        for (var cfOption in cfOptions) {
          if (cfOptions[cfOption].tagName === "OPTGROUP") {
            optGroup = cfOptions[cfOption].label;
          }
          if (cfOptions[cfOption].tagName === "OPTION") {
            optValue = cfOptions[cfOption].value;
            if (optValue) {
              customFields[optGroup + '-' + optValue] = optValue;
              customFieldsString += optGroup + '-' + optValue + ':' + optValue + ';';
            }
          }
        }
        var cfComboOptions = temp.querySelectorAll('select[name="List-Object-RT::Ticket-' + getTicketId() + '-CustomField-' + cfId + '-Value"] *');
        for (var cfComboOption in cfComboOptions) {
          if (cfComboOptions[cfComboOption].tagName === "OPTION") {
            optValue = cfComboOptions[cfComboOption].value;
            if (optValue) {
              customFields[optGroup + '-' + optValue] = optValue;
              customFieldsString += optGroup + '-' + optValue + ':' + optValue + ';';
            }
          }
        }
        removeElement(temp);

        // cache results
        oldValue = getValue('customFields-' + cfId, '');
        setValue('customFields-' + cfId, customFieldsString);
        setValue('customFields-' + cfId + '-timestamp', new Date().getTime()/1000);
        if (oldValue !== customFieldsString) {
          location.reload();
        }
      }
    });
  }

  return getObject('customFields-' + cfId);
}

function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
  var regexS = "[\\?&]" + name + "=([^&#]*)";
  var regex = new RegExp(regexS);
  var results = regex.exec(document.location.search);
  if(!results)
    return "";
  else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
}

/**
 * Initialize
 * @return void
 */
function init() {
  var first;
  var container;
  var supercontainer;

  // add version information in footer
  (function addVersion() {
    container = document.querySelector('p#bpscredits');

    container.appendChild(document.createElement('br'));
    version = document.createElement('span');
    container.appendChild(version);
    version.innerHTML = "RThelper " + info.version + ", Copyright " + (new Date().getFullYear()) + " <a href='https://gitlab.ebi.ac.uk/ebiwd/rthelper'>EMBL-EBI</a>.";
  })();

  // focus on element named in Focus query string
  (function doFocus() {
    focusElement = getParameterByName('Focus');
    if (!focusElement) {
      return;
    }

    element = document.querySelector('[name="' + focusElement + '"]');
    if (!element) {
      return;
    }

    element.focus();
  })();

  // click on element named in Click query string
  (function doClick() {
    var clickElement = getParameterByName('Click');
    if (!clickElement) {
      return;
    }

    var element = document.querySelector('[name="' + clickElement + '"]');
    if (!element) {
      return;
    }

    element.click();
  })();

  // apply login and password to login box
  (function autoLogin() {
    if (!document.querySelector('form#login') || document.querySelector('ul.action-results li') || document.referrer.indexOf(info.path + '/NoAuth/Logout.html') !== -1) {
      return;
    }

    username = getValue('loginUsername', '');
    password = getValue('loginPassword', '').decode();
    if (username !== '') {
      document.querySelector('input[name="user"]').value = username;
      if (password !== '') {
        document.querySelector('input[name="pass"]').value = password;
        document.querySelector('input[type="submit"]').click();
      }
      else {
        document.querySelector('input[name="pass"]').focus();
      }
    }
    else {
      document.querySelector('input[name="user"]').focus();
    }
  })();

  // redirect to settings page on first use
  (function firstUse() {
    if (document.getElementById('not-logged-in')) {
      return;
    }
//    if (getValue('showOwners', '') === '' || getValue('showQueues', '') === '' || getValue('showStatuses', '') === '') {
    if (getValue('showQueues', '') === '' || getValue('showStatuses', '') === '') {
      if (document.location.pathname !== info.path + '/rthelper') {
        document.location.href = info.path + '/rthelper';
      }
    }
  })();

  // add some links to queue
  (function queueSwitch() {
    if (document.location.pathname !== info.path + '/Ticket/Display.html') {
      return;
    }

    var currentQueue = getQueue();
    var queues = getQueues();
    var queue;
    container = document.querySelector('div.ticket-info-basics div.queue div.value');

    if (!container) {
      return;
    }
    first=true;
    for (queue in queues) {
      if (queues[queue]) {
        insertLink(
          container,
          queue,
          buildLink(
            getModifyLink({'Queue':queues[queue]}),
            refreshElement('#body')
          ),
          null,
          null,
          first
        ).className = 'rthelper-button' + (queue === currentQueue ? ' rthelper-active' : '' );
        first = false;
      }
    }
  })();

  // add some links to status
  (function statusSwitch() {
    if (document.location.pathname !== info.path + '/Ticket/Display.html') {
      return;
    }

    var currentStatus = getStatus();
    var statuses = getStatuses();
    var status;
    container = document.querySelector('div.ticket-info-basics div.status div.value');
    if (!container) {
      return;
    }
    first=true;
    for (status in statuses) {
      if (statuses[status]) {
        insertLink(
          container,
          status,
          buildLink(
            getModifyLink({'Status':statuses[status]}),
            refreshElement('#body')
          ),
          null,
          null,
          first
        ).className = 'rthelper-button' + (statuses[status] === currentStatus ? ' rthelper-active' : '' );
        first = false;
      }
    }
  })();

  // add some links to owner
  (function ownerSwitch() {
    if (document.location.pathname !== info.path + '/Ticket/Display.html') {
      return;
    }

    var currentOwner = getOwner();
    var owners = getOwners();
    var owner;
    var button;
    var firstname;
    container = document.querySelector('div.ticket-info-people div.value');
    if (!container) {
      return;
    }
    first=true;
    for (owner in owners) {
      if (owners[owner]) {
        button = insertLink(
          container,
          (firstname = owner.split(' ',1)[0]) + (firstname!=='Nobody' ? (' ' + owner.split(' ',2)[1][0]) : ''), // shorten name
          buildLink(
            getModifyLink({'Owner':owners[owner],'ForceOwnerChange':1}),
            refreshElement('#body')
          ),
          null,
          null,
          first
        );
        button.className = 'rthelper-button' + (owner === currentOwner ? ' rthelper-active' : '' );
        button.title = owner;
        first = false;
      }
    }
  })();

  // add some links to project
  (function customFieldSwitch() {
    if (document.location.pathname !== info.path + '/Ticket/Display.html') {
      return;
    }

    var customFields = document.querySelectorAll('div.ticket-info-cfs div.custom-field');

    for (var customField in customFields) {
      if (customFields[customField].id) {
        var customFieldId = customFields[customField].id.split('-')[1];
        var currentCustomFieldValue = getCustomField(customFieldId);
        var params = {};
        var customFieldValues = getCustomFields(customFieldId);
        var customFieldValue;
        container = document.querySelector('div#CF-' + customFieldId + '-ShowRow div.value');
        if (!container) {
          return;
        }
        first=true;
        var customFieldGroup = '';
        for (customFieldValue in customFieldValues) {
          if (customFieldValues[customFieldValue]) {
            if (customFieldGroup != customFieldValue.split('-',1)[0]) {
//              first = true;
              customFieldGroup = customFieldValue.split('-',1)[0];
            }
            params['Object-RT::Ticket-' + getTicketId() + '-CustomField-' + customFieldId + '-Values'] = (customFieldValues[customFieldValue] === '(no value)' ? ' ' : customFieldValues[customFieldValue]); // build dynamic object key
            insertLink(
              container,
              customFieldValue.tokenize('-',2)[1],
              buildLink(
                getModifyLink(params),
                refreshElement('#body')
              ),
              null,
              null,
              first
            ).className = 'rthelper-button' + (customFieldValues[customFieldValue] === currentCustomFieldValue ? ' rthelper-active' : '' );
            first = false;
          }
        }
      }
    }
  })();

  (function hideCurrentSelection() {
    var active = document.querySelectorAll('div .rthelper-active');
    var i;
    for (i=0; i<active.length; i++) {
      active[i].parentNode.className += ' hide-first-line';
    }

    var hides = document.querySelectorAll('.hide-first-line');

  })();

  // add quick link to add cc
  (function addCcLink() {
    if (document.location.pathname !== info.path + '/Ticket/Display.html') {
      return;
    }

    // use .right if it exists
    container = document.querySelector('div.ticket-info-people .titlebox-title .right');
    if (!container) {
      // create .right
      supercontainer = document.querySelector('div.ticket-info-people .titlebox-title');
      if (!supercontainer) {
        return;
      }
      container = document.createElement('span');
      container.className = 'right';
      supercontainer.appendChild(container);
    }
    else {
      insertLink(container, ' | ');
    }
    insertLink(
      container,
      'CC me',
      buildLink(
        getModifyPeopleLink({'WatcherTypeEmail1':'Cc','WatcherAddressEmail1':getUserId()}),
        refreshElement('#body')
      )
    );
  })();

  (function addCcInput() {
    if (document.location.pathname !== info.path + '/Ticket/Display.html') {
      return;
    }

    var types = {'Requestor': 1, 'Cc': 2, 'AdminCc': 3};

    function personFormSubmit() {
      var action = getModifyPeopleLink({
        'WatcherTypeEmail1': this.WatcherTypeEmail1.value,
        'WatcherAddressEmail1': this.WatcherAddressEmail1.value
      });
      this.action = buildLink(action, refreshElement());
      return true;
    }

    for (var type in types) { // change to i<=3 to include AdminCc
      container = document.querySelectorAll('div.ticket-info-people div.value')[types[type]];
      if (!container) {
        return;
      }

      // create the form
      var addPersonForm = document.createElement('form');
      container.appendChild(addPersonForm);
      addPersonForm.action = '#';
      addPersonForm.method = 'post';
      addPersonForm.onsubmit = personFormSubmit;
      var labelAddress = document.createElement('label');
      addPersonForm.appendChild(labelAddress);
      labelAddress.innerHTML = '<span class="rthelper-button">Add ' + type + '</span>'; // + type +;
      labelAddress.onclick = function() {
        this.childNodes[1].style.visibility = 'visible';
        this.childNodes[1].focus();
        return false;
      };
      var inputAddress = document.createElement('input');
      labelAddress.appendChild(inputAddress);
      inputAddress.type = 'text';
      inputAddress.name = 'WatcherAddressEmail1';
      inputAddress.title = 'Type email address and press enter';
      inputAddress.style.visibility = 'hidden';
      inputAddress.onblur = function() {
        if (!this.value) {
          this.style.visibility = 'hidden';
        }
        return false;
      };
      var inputType = document.createElement('input');
      addPersonForm.appendChild(inputType);
      inputType.type = 'hidden';
      inputType.name = 'WatcherTypeEmail1';
      inputType.value = type;
    }
  })();

  // add quick link to reply
  (function quickreplyLink() {
    if (document.location.pathname !== info.path + '/Ticket/Display.html') {
      return;
    }

    container = document.querySelector('div.ticket-info-basics .titlebox-title .right');
    if (!container) {
      supercontainer = document.querySelector('div.ticket-info-basics .titlebox-title');
      if (!supercontainer) {
        return;
      }
      container = document.createElement('span');
      container.className = 'right';
      supercontainer.appendChild(container);
    }
    else {
      insertLink(container, ' | ');
    }
    insertLink(
      container,
      'Quick reply/resolve',
      "javascript:(document.location=document.querySelector('.actions .reply-link').href+'&Status=resolved&Focus=UpdateContent')"
    );
  })();

  // add quick link to SPAM
  (function spamLink() {
    if (document.location.pathname !== info.path + '/Ticket/Display.html') {
      return;
    }

    container = document.querySelector('div.ticket-info-basics .titlebox-title .right');
    if (!container) {
      supercontainer = document.querySelector('div.ticket-info-basics .titlebox-title');
      if (!supercontainer) {
        return;
      }
      container = document.createElement('span');
      container.className = 'right';
      supercontainer.appendChild(container);
    }
    else {
      insertLink(container, ' | ');
    }
    insertLink(
      container,
      'Report Spam',
      "javascript:(function(){" +
      "links=document.querySelectorAll('.actions .forward-link');fw=window.open(links[links.length-1].href+'&To=is-spam@labs.sophos.com&Cc=rt-spam@ebi.ac.uk&Content=Reporting+as+spam&Click=Forward','_blank');wait=setInterval(function(){if(fw.document.location.pathname=='/Ticket/Forward.html'&&fw.document.location.search==''){" +
      "clearInterval(wait);fw.close();" +
      "mod=document.querySelector('a#page-basics').href;jQuery.ajax({url:mod+'&Status=deleted',cache:false,complete:function(){location.reload();}})" +
      "}},1000);" +
      "})()"
    );
  })();

  // add style
  (function createStyles() {
    document.getElementById('logo').innerHTML  = '<a href="http://www.ebi.ac.uk"><img src="//www.ebi.ac.uk/web_guidelines/images/logos/EMBL-EBI/EMBL_EBI_Logo_white.png" /></a> <span class="rtname"><a href="' + info.path + '/rthelper">RThelper admin</a></span>';

    function setMenuHeight() {
      document.getElementById('body').style.paddingTop = (document.getElementById('page-menu').clientHeight + 2) + 'px';
    }

    if (document.getElementById('page-menu') && document.getElementById('body')) {
      setInterval(setMenuHeight, 1000);
    }

    var head = document.head || document.getElementsByTagName('head')[0];
    var style = document.createElement('style');

    style.type = 'text/css';
    var css = '' +
      'html { overflow-y: scroll; }' +
      '.browser-firefox input[type="reset"], input[type="submit"], input[class="button"], button { border-radius: 0.5em; }' + // fix button styling
      '.browser-chrome .titlebox .titlebox-title .left { line-height: 1.6em; }' + // fix height of titlebox
      '.rthelper-button { color: #222 !important; background: #eee; padding: 0.25em; padding-left: 0.5em; padding-right: 0.5em; border-radius: 0.5em; -moz-border-radius: 0.5em; -webkit-border-radius: 0.5em; display: inline-block; margin: 0.125em; border: 1px solid #ddd; }' +
      '.rthelper-active { background: #369; color: #fff !important; }' +
      '.ticket-info-basics .label, .ticket-info-cfs .label, .ticket-info-people .label { vertical-align: top; padding-top: 0.375em}' + // fix position of labels
      '.ticket-info-basics .labeltop, .ticket-info-cfs .labeltop, .ticket-info-people .labeltop { vertical-align: top; padding-top: 0.375em}' + // fix position of labels
      'ul#app-nav>li>a { color: white !important; }' +
      'div#body { margin-left: 0; border: none; border-radius: 0; padding-top: 30px; }' +
      'div#quickbar { background: #182c5d; color: #fff; border: none; }' +
      'body.aileron #main-navigation #app-nav > li, body.aileron #main-navigation #app-nav > li > a, #prefs-menu > li, #prefs-menu > li > a, #logo .rtname { color: #fff }' +
      'div#logo span.rtname a { color: #fff; font-weight: bold; }' +
      'body#comp-rthelper-admin fieldset { margin-top: 1em; width: 48%; }' +
      'body#comp-rthelper-admin fieldset legend { font-weight: bold; }' +
      '#topactions a.rthelper-button { float: right; }' +
      'div#footer #bpscredits { background: none; padding-top: 0; }' +
      '';

    if (style.styleSheet){
      style.styleSheet.cssText = css;
    } else {
      style.appendChild(document.createTextNode(css));
    }

    head.appendChild(style);
  })();

  (function browserIdentify() {
    if (navigator.userAgent.indexOf("Firefox") !== -1) {
      document.body.parentNode.className += ' browser-firefox';
    }
    if (navigator.userAgent.indexOf("Chrome") !== -1) {
      document.body.parentNode.className += ' browser-chrome';
    }
  })();

  (function addQuickSearch() {
    var quickSearch;
    var links, link;

    // do not do anything if not logged in
    if (document.getElementById('not-logged-in')) {
      return;
    }

    // get fresh set of quicksearches every time we visit homepage
    if (document.location.pathname === info.path + '/' && document.querySelector('.myrt')) {
      quickSearch = '';
      links = document.querySelectorAll('.titlebox .left a');

      for (link in links) {
        if (links[link].href) {
          href = links[link].href;
          text = links[link].innerHTML;
          quickSearch += '<a class="rthelper-button" href="' + href + '">' + text + '</a> ';
        }
      }

      setValue('quickSearch', quickSearch);
    }

    // insert quicksearch into top of page
    quickSearch = document.createElement('div');
    quickSearch.id = 'quicksearch';
    quickSearch.innerHTML = getValue('quickSearch', '');
    body = document.getElementById('body');
    skipNav = document.getElementById('skipnav');
    body.insertBefore(quickSearch, skipNav);

    // highlight the active search
    links = document.querySelectorAll('#quicksearch a');
    for (link in links) {
      if (links[link].href === document.location.href) {
        links[link].className += ' rthelper-active';
      }
    }
  })();

  (function adminPage() {
    if (document.location.pathname !== info.path + '/rthelper') {
      return;
    }

    var owners = {}, statuses = {}, queues = {};

    // remove page not found rubbish, replace title
    removeElement(document.querySelector('#body h1').nextSibling);
    removeElement(document.querySelector('#body h1'));
    document.querySelector('#header h1').innerHTML = 'RThelper admin';
    document.title = 'RThelper admin';
    document.body.id = 'comp-rthelper-admin';

    function renderQueues(response) {
      // put into dom temporarily
      var temp = document.createElement('div');
      temp.innerHTML = response.responseText;

      var queueOptions = temp.querySelectorAll('select[name="Queue"] option');
      for (var queueOption in queueOptions) {
        if (queueOptions[queueOption].value && queueOptions[queueOption].text) {
          queues[queueOptions[queueOption].text] = queueOptions[queueOption].value;
        }
      }
      removeElement(temp);

      // insert queue options in form
      for (var queue in queues) {
        var queueInput = document.createElement('input');
        var queueLabel = document.createElement('label');
        queueInput.type = 'checkbox';
        queueInput.name = 'showQueues';
        queueInput.value = queue + ':' + queues[queue];
        if (queueDefault.indexOf(queueInput.value) !== -1) {
          queueInput.checked = true;
        }
        if (queue === 'General') {
          queueInput.disabled = true;
          queueInput.checked = true;
        }
        queueLabel.appendChild(queueInput);
        queueLabel.appendChild(document.createTextNode(queue));
        queueFieldset.appendChild(queueLabel);
        queueFieldset.appendChild(document.createElement('br'));

        GM_xmlhttpRequest({
          method: "GET",
          url: info.path + '/Ticket/Create.html?Queue=' + queues[queue],
          headers: {
            'Referer': info.domain + info.path
          },
          onload: renderUsers
        });
      }
    }

    function renderUsers(response) {
      // put into dom temporarily
      var temp = document.createElement('div');
      temp.innerHTML = response.responseText;

      var ownerOptions = temp.querySelectorAll('select[name="Owner"] option');
      for (var ownerOption in ownerOptions) {
        if (ownerOptions[ownerOption].value && ownerOptions[ownerOption].text) {
          if (ownerOptions[ownerOption].text.indexOf('(') !== -1 && ownerOptions[ownerOption].text.indexOf(')') !== -1) {
            owners[/\((.*)\)/gi.exec(ownerOptions[ownerOption].text)[1]] = ownerOptions[ownerOption].value;
          }
          else {
            owners[ownerOptions[ownerOption].text] = ownerOptions[ownerOption].value;
          }

        }
      }
      removeElement(temp);

      // insert user options in form

      for (var owner in owners) {
        var ownerInput = document.createElement('input');
        var ownerLabel = document.createElement('label');
        ownerInput.type = 'checkbox';
        ownerInput.name = 'showOwners';
        ownerInput.value = owner + ':' + owners[owner];
        if (ownerDefault.indexOf(ownerInput.value) !== -1) {
          ownerInput.checked = true;
        }
        if (owner === 'Nobody in particular' || owner === getName(true)) {
          ownerInput.disabled = true;
          ownerInput.checked = true;
        }
        if (document.querySelectorAll('input[value="' + owner + ':' + owners[owner] + '"]').length < 1) {
          ownerLabel.appendChild(ownerInput);
          ownerLabel.appendChild(document.createTextNode(owner));
          ownerFieldset.appendChild(ownerLabel);
          ownerFieldset.appendChild(document.createElement('br'));
        }
      }
    }

    // load preferences pane
    GM_xmlhttpRequest({
      method: "GET",
      url: getElementText('a#preferences', 'href'),
      onload: renderQueues
    });

    // provide static list for statusOptions, as order is inconsistent and data will not change
    var statusOptions = [
      { 'value': 'new', 'text': 'New' },
      { 'value': 'open', 'text': 'Open' },
      { 'value': 'stalled', 'text': 'Stalled' },
      { 'value': 'rejected', 'text': 'Rejected' },
      { 'value': 'deleted', 'text': 'Deleted' },
      { 'value': 'resolved', 'text': 'Resolved' }
    ];
    for (var statusOption in statusOptions) {
      if (statusOptions[statusOption].value && statusOptions[statusOption].text) {
        statuses[statusOptions[statusOption].text] = statusOptions[statusOption].value;
      }
    }



    // create option form
    var form = document.createElement('form');
    form.action = info.path + '/rthelper';
    form.method = 'put';
    document.getElementById('body').appendChild(form);
    // store the form results
    form.onsubmit = function() {

      var loginPassword = this.querySelector('input[name="password"]');
      setValue('loginPassword', loginPassword.value.encode());

      var showOwners = this.querySelectorAll('input[name="showOwners"]');
      var showOwnersString = '';
      for (var showOwner in showOwners) {
        if (showOwners[showOwner].checked) {
          showOwnersString += showOwners[showOwner].value + ';';
        }
      }
      setValue('showOwners', showOwnersString);

      var showStatuses = this.querySelectorAll('input[name="showStatuses"]');
      var showStatusesString = '';
      for (var showStatus in showStatuses) {
        if (showStatuses[showStatus].checked) {
          showStatusesString += showStatuses[showStatus].value + ';';
        }
      }
      setValue('showStatuses', showStatusesString);

      var showQueues = this.querySelectorAll('input[name="showQueues"]');
      var showQueuesString = '';
      for (var showQueue in showQueues) {
        if (showQueues[showQueue].checked) {
          showQueuesString += showQueues[showQueue].value + ';';
        }
      }
      setValue('showQueues', showQueuesString);

      alert('Settings saved');
      return false;
    };

    // populate the form
    var loginFieldset = document.createElement('fieldset');
    form.appendChild(loginFieldset);
    var loginLegend = document.createElement('legend');
    loginFieldset.appendChild(loginLegend);
    loginLegend.innerHTML = 'Automatic login [OPTIONAL]';
    var loginIntro = document.createElement('p');
    loginFieldset.appendChild(loginIntro);
    loginIntro.innerHTML = 'Enter your RT password to enable automatic login.';

    setValue('loginUsername', getElementText('span.current-user'));

    var passwordDefault = getValue('loginPassword', '').decode();
    var passwordInput = document.createElement('input');
    var passwordLabel = document.createElement('label');
    passwordInput.type = 'password';
    passwordInput.name = 'password';
    passwordInput.value = passwordDefault;
    passwordLabel.appendChild(document.createTextNode('Password: '));
    passwordLabel.appendChild(passwordInput);
    loginFieldset.appendChild(passwordLabel);
    loginFieldset.appendChild(document.createElement('br'));

    var loginOuttro = document.createElement('p');
    loginFieldset.appendChild(loginOuttro);
    loginOuttro.innerHTML = 'NB: Your password will be encoded and stored in <a href="http://www.w3schools.com/html/html5_webstorage.asp">localstorage</a> on your computer. This username/password will only accessible within the ' + info.domain + ' domain.';

    var statusFieldset = document.createElement('fieldset');
    form.appendChild(statusFieldset);
    var statusLegend = document.createElement('legend');
    statusFieldset.appendChild(statusLegend);
    statusLegend.innerHTML = 'Status selection';
    var statusIntro = document.createElement('p');
    statusFieldset.appendChild(statusIntro);
    statusIntro.innerHTML = 'Select the statuses you wish to switch between.';
    var statusDefault = getValue('showStatuses', '');

    var queueFieldset = document.createElement('fieldset');
    form.appendChild(queueFieldset);
    var queueLegend = document.createElement('legend');
    queueFieldset.appendChild(queueLegend);
    queueLegend.innerHTML = 'Queue selection';
    var queueIntro = document.createElement('p');
    queueFieldset.appendChild(queueIntro);
    queueIntro.innerHTML = 'Select the queues you wish to switch between - typically the queues your team is responsible for.';
    var queueDefault = getValue('showQueues', '');

    var ownerFieldset = document.createElement('fieldset');
    form.appendChild(ownerFieldset);
    var ownerLegend = document.createElement('legend');
    ownerFieldset.appendChild(ownerLegend);
    ownerLegend.innerHTML = 'Owner selection';
    var ownerIntro = document.createElement('p');
    ownerFieldset.appendChild(ownerIntro);
    ownerIntro.innerHTML = 'Select the users you wish to switch between - typically the users in your team.';
    var ownerDefault = getValue('showOwners', '');

    for (var status in statuses) {
      var statusInput = document.createElement('input');
      var statusLabel = document.createElement('label');
      statusInput.type = 'checkbox';
      statusInput.name = 'showStatuses';
      statusInput.value = status + ':' + statuses[status];
      if (statusDefault.indexOf(statusInput.value) !== -1) {
        statusInput.checked = true;
      }
      if (status === 'New' || status === 'Open' || status === 'Resolved') {
        statusInput.disabled = true;
        statusInput.checked = true;
      }
      statusLabel.appendChild(statusInput);
      statusLabel.appendChild(document.createTextNode(status));
      statusFieldset.appendChild(statusLabel);
      statusFieldset.appendChild(document.createElement('br'));
    }

    // submit button
    formSubmit = document.createElement('input');
    form.appendChild(formSubmit);
    formSubmit.type = 'submit';
  })();
}

try {
  init();
}
catch(err) {
  console.log(err.message);
}
